# Baseline of ESLint Configurations for 8 Hobbies Projects

[![npm version](https://badge.fury.io/js/@8hobbies%2Feslint-conf-baseline.svg)](https://badge.fury.io/js/@8hobbies%2Feslint-conf-baseline)
[![pipeline status](https://gitlab.com/8hobbies/eslint-conf-baseline/badges/master/pipeline.svg)](https://gitlab.com/8hobbies/eslint-conf-baseline/-/commits/master)

[GitLab](https://gitlab.com/8hobbies/eslint-conf-baseline) | [GitHub](https://github.com/8hobbies/eslint-conf-baseline)

To use it, extends the rules exported by this extension.

Example:

```js
// @ts-check

import configs from "@8hobbies/eslint-conf-baseline";
import globals from "globals";

export default [
  ...configs.recommended,
  {
    files: ["**/*.ts"],
    ignores: configs.ignores,
    languageOptions: {
      globals: {
        ...globals.node,
      },
      parserOptions: {
        project: ["tsconfig.json"],
        tsconfigRootDir: import.meta.dirname,
      },
    },
  },
];
```

## License

    Copyright (C) 2024 8 Hobbies, LLC <hong@8hobbies.com>

    Permission to use, copy, modify, and/or distribute this software for anypurpose with or without fee
    is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIESWITH REGARD TO THIS SOFTWARE
    INCLUDING ALL IMPLIED WARRANTIES OFMERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE
    LIABLE FORANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGESWHATSOEVER RESULTING
    FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN ANACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
    ACTION, ARISING OUT OFOR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
