/** @license 0BSD
  Copyright (C) 2024 8 Hobbies, LLC <hong@8hobbies.com>

  Permission to use, copy, modify, and/or distribute this software for anypurpose with or without
  fee is hereby granted.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIESWITH REGARD TO THIS
  SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OFMERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
  AUTHOR BE LIABLE FORANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY
  DAMAGESWHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN ANACTION OF CONTRACT,
  NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OFOR IN CONNECTION WITH THE USE OR PERFORMANCE OF
  THIS SOFTWARE.
 */
import eslint from "@eslint/js";
import tseslint from "typescript-eslint";

const ignores = ["dist/*", "docs/*"] as const;
const tsFiles = ["**/*.ts", "**/*.mts", "**/*.tsx"] as const;

const recommended = [
  {
    ignores,
  },
  eslint.configs.recommended,
  {
    files: [...tsFiles, "**/*.js", "**/*.mjs"],
    rules: {
      "no-constant-binary-expression": ["error"],
      "no-constructor-return": ["error"],
      "no-duplicate-imports": ["error"],
      "no-new-native-nonconstructor": ["error"],
      "no-self-compare": ["error"],
      "no-template-curly-in-string": ["error"],
      "no-unneeded-ternary": ["error"],
      "no-unreachable-loop": ["error"],
      "no-unused-private-class-members": ["error"],
      "sort-imports": ["error"],
      "unicode-bom": ["error", "never"],
    },
  },
  // Apply ts rules only to ts files
  ...[
    ...tseslint.configs.strictTypeChecked,
    ...tseslint.configs.stylisticTypeChecked,
  ].map((elem) => ({
    ...elem,
    files: tsFiles,
  })),
  {
    files: tsFiles,
    languageOptions: {
      parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module",
      },
    },
    rules: {
      "@typescript-eslint/consistent-type-assertions": [
        "error",
        {
          assertionStyle: "never",
        },
      ],
      "@typescript-eslint/explicit-function-return-type": "error",
      "@typescript-eslint/no-misused-promises": [
        "error",
        {
          checksVoidReturn: false,
        },
      ],
      "@typescript-eslint/no-unused-vars": [
        "error",
        {
          argsIgnorePattern: "^_",
          varsIgnorePattern: "^_",
          caughtErrorsIgnorePattern: "^_",
        },
      ],
      "@typescript-eslint/no-unnecessary-type-assertion": [
        "error",
        { typesToIgnore: ["const"] },
      ],
      "@typescript-eslint/restrict-template-expressions": [
        "error",
        {
          allowNumber: true,
        },
      ],
      // This must be off otherwise "@typescript-eslint/no-unused-expressions"
      // may not work properly. See the doc of
      // @typescript-eslint/no-unused-expressions.
      "no-unused-expressions": "off",
      "@typescript-eslint/no-unused-expressions": "error",
    },
  },
] as const;

export default {
  recommended,
  ignores,
};
